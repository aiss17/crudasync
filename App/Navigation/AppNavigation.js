//Library navigation
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

//Pages
import Splash from "../Pages/Splash";
import HomeNavigation from './HomeNavigation';
import Member from '../Pages/HomePages/Member'
import Instansi from '../Pages/HomePages/Instansi'

const AppNavigator = createStackNavigator({
    Splash: { screen: Splash, navigationOptions: { headerShown: false }},
    HomeNavigation: { screen: HomeNavigation, navigationOptions: { headerShown: false }},
    Member: { screen: Member, navigationOptions: { headerShown: false }},
    Instansi: { screen: Instansi, navigationOptions: { headerShown: false }},
}, {
    initialRouteName: 'Splash'
})

export default createAppContainer(AppNavigator);