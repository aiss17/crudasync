import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from 'react'
import { BackHandler, Text, View, TouchableOpacity, Alert, StyleSheet, FlatList, Image, TextInput } from 'react-native'
import { NavigationEvents } from 'react-navigation'
import Images from '../../Assets/Images';
import Icon from 'react-native-vector-icons/AntDesign'
import Modal from 'react-native-modal';

class Instansi extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataInstansi: [],
            backUpListInstansi: [],
            visibleEdit: false,
            visibleAdd: false,
            id: '',
            editName: '',
            editAlamat: '',
            noTelp: '',
            tempName: ''
        }
    }

    backPressed = () => {
		Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
        )
		return true;
    };

    deletePressed = (id) => {
		Alert.alert(
            'Delete Instance',
            'Are you sure want to delete this Instance?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'Delete',
                onPress: () => this.deleteDataInstansi(id)
            }, ], {
                cancelable: false
            }
        )
		return true;
    };

    componentDidMount() {
        this.getDataInstansis()
		BackHandler.addEventListener('hardwareBackPress', this.backPressed);
	}
	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }

    async getDataInstansis() {
        let backUpListInstansi = await AsyncStorage.getItem("dataInstansi")
        console.log(await AsyncStorage.getItem('dataInstansi'))
        
        setTimeout(() => {
            this.setState({
                dataInstansi: JSON.parse(backUpListInstansi)
            })
        }, 2000);
    }

    async addDataInstansi() {
        let dataInstansi = await AsyncStorage.getItem('dataInstansi')

        console.log(await AsyncStorage.getItem('dataInstansi'))
        let parseData = JSON.parse(dataInstansi)

        if(parseData != null) {
            let indexID = parseData.findIndex((val) => val.nama == this.state.editName)

            if (indexID != -1) {
                alert("Instance has been registered!")
            } else {
                const dataInstansi = 
                {
                    nama: this.state.editName,
                    alamat: this.state.editAlamat,
                    noTelp: this.state.noTelp
                }
        
                parseData.push(dataInstansi)
                const dataInstansiParse = JSON.stringify(parseData)
                this.setItem("dataInstansi", dataInstansiParse)
                setTimeout(() => {
                    this.setState({
                        visibleAdd: false,
                        editName: '',
                        editAlamat: '',
                        noTelp: ''
                    })
                }, 2000);
            }
        } else {
            const dataInstansi = [ 
                {
                    nama: this.state.editName,
                    alamat: this.state.editAlamat,
                    noTelp: this.state.noTelp
                }
            ]
    
            const dataInstansiParse = JSON.stringify(dataInstansi)
            this.setItem("dataInstansi", dataInstansiParse)
        }
        setTimeout(() => {
            this.setState({
                visibleAdd: false,
                editName: '',
                editAlamat: '',
                noTelp: ''
            })
        }, 2000);
    }

    setItem = async (key, item) => {
        try {
            await AsyncStorage.setItem(key, item);
            console.log("Data baru yang dibuat => " + await AsyncStorage.getItem(key))
            this.getDataInstansis()
        } catch(err) {
            console.log("Something wrong => " + err)
        }
    }

    async putDataInstansi(nama, alamat) {
        let dataInstansi = await AsyncStorage.getItem('dataInstansi')

        console.log(await AsyncStorage.getItem('dataInstansi'))
        let parseData = JSON.parse(dataInstansi)
        let indexData = parseData.findIndex((val) => val.nama == this.state.tempName)

        if (indexData != -1) {
            parseData.splice(indexData, 1)

            const dataInstansi = 
            {
                nama: nama,
                alamat: alamat,
                noTelp: noTelp
            }

            parseData.push(dataInstansi)
            const dataInstansiParse = JSON.stringify(parseData)
            this.setItem("dataInstansi", dataInstansiParse)
            setTimeout(() => {
                this.setState({
                    visibleEdit: false,
                    editName: '',
                    editAlamat: '',
                    noTelp: ''
                })
            }, 2000);
        } else {
            alert("gagal!")
        }
    }

    async deleteDataInstansi(nama) {
        let dataInstansi = await AsyncStorage.getItem('dataInstansi')

        console.log(await AsyncStorage.getItem('dataInstansi'))
        let parseData = JSON.parse(dataInstansi)
        let indexData = parseData.findIndex((val) => val.nama == nama)

        parseData.splice(indexData, 1)
        const dataInstansiParse = JSON.stringify(parseData)
        this.setItem("dataInstansi", dataInstansiParse)
    }

    render() {
        return(
            <View style={styles.container}>
                <NavigationEvents            
                    onDidFocus={() => this.getDataInstansis()}
                />

                <Modal isVisible={this.state.visibleEdit} 
                    style={styles.modal}
                    onBackButtonPress={() => this.setState({ visibleEdit: false })}
                >
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'flex-start' }}>
                            <Text style={{ fontSize: 30, fontWeight: 'bold', marginRight: 5}}>Edit</Text>
                            <Icon 
                                name='edit' 
                                size={20} 
                                style={{justifyContent: 'center', alignSelf: 'center'}}
                            />
                        </View>

                        <View style={[styles.frame, { marginTop: 20 }]}>
                            <Text style={styles.label}>
                                Name Instance
                            </Text>
                            <TextInput
                                ref={(input) => { this.editName = input; }}
                                keyboardType={'default'}
                                onSubmitEditing={() => { this.editAlamat.focus(); }}
                                editable={true}
                                style={styles.textInput}
                                multiline={false}
                                returnKeyType={'next'}
                                value={this.state.editName}
                                onChangeText={(text) => this.setState({ editName: text })}
                                placeholder={"Enter your Instance Name..."}
                            />
                        </View>

                        <View style={styles.frame}>
                            <Text style={styles.label}>
                                Alamat
                            </Text>
                            <TextInput
                                ref={(input) => { this.editAlamat = input; }}
                                keyboardType={'default'}
                                editable={true}
                                style={styles.textInput}
                                multiline={false}
                                returnKeyType={'done'}
                                value={this.state.editAlamat}
                                onChangeText={(text) => this.setState({ editAlamat: text })}
                                placeholder={"Enter address..."}
                            />
                        </View>

                        <View style={styles.frame}>
                            <Text style={styles.label}>
                                Nomor Telepon
                            </Text>
                            <TextInput
                                ref={(input) => { this.noTelp = input; }}
                                keyboardType={'phone-pad'}
                                editable={true}
                                style={styles.textInput}
                                multiline={false}
                                maxLength={13}
                                returnKeyType={'done'}
                                value={this.state.noTelp}
                                onChangeText={(text) => this.setState({ noTelp: text.replace(/[^0-9]/g, '') })}
                                placeholder={"example : 89923XXXX"}            
                            />
                        </View>

                        <TouchableOpacity
                            activeOpacity = {.7}
                            style = {styles.button}
                            onPress={() => this.putDataInstansi(this.state.editName, this.state.editAlamat, this.state.noTelp)}
                        >
                            <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>
                                Save
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity = {.7}
                            style = {styles.buttonCancel}
                            onPress={() => this.setState({
                                visibleEdit: false
                            })}
                        >
                            <Text style={{textAlign: 'center', fontSize: 15}}>
                                Cancel
                            </Text>
                        </TouchableOpacity>
                    </View>
                </Modal>

                <Modal isVisible={this.state.visibleAdd} 
                    style={styles.modal}
                    onBackButtonPress={() => this.setState({ visibleAdd: false })}
                >
                    <View style={{ flex: 1 }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'flex-start' }}>
                            <Text style={{ fontSize: 30, fontWeight: 'bold', marginRight: 5}}>Add</Text>
                            <Icon 
                                name='addfile' 
                                size={20} 
                                style={{justifyContent: 'center', alignSelf: 'center'}}
                            />
                        </View>

                        <View style={[styles.frame, { marginTop: 20 }]}>
                            <Text style={styles.label}>
                                Name Instance
                            </Text>
                            <TextInput
                                ref={(input) => { this.editName = input; }}
                                keyboardType={'default'}
                                onSubmitEditing={() => { this.editAlamat.focus(); }}
                                editable={true}
                                style={styles.textInput}
                                multiline={false}
                                returnKeyType={'next'}
                                value={this.state.editName}
                                onChangeText={(text) => this.setState({ editName: text })}
                                placeholder={"Enter name of Instance..."}
                            />
                        </View>

                        <View style={styles.frame}>
                            <Text style={styles.label}>
                                Alamat
                            </Text>
                            <TextInput
                                ref={(input) => { this.editAlamat = input; }}
                                keyboardType={'default'}
                                editable={true}
                                style={styles.textInput}
                                multiline={false}
                                returnKeyType={'done'}
                                value={this.state.editAlamat}
                                onChangeText={(text) => this.setState({ editAlamat: text })}
                                placeholder={"Enter address of Instance..."}
                            />
                        </View>

                        <View style={styles.frame}>
                            <Text style={styles.label}>
                                Nomor Telepon
                            </Text>
                            <TextInput
                                ref={(input) => { this.noTelp = input; }}
                                keyboardType={'phone-pad'}
                                editable={true}
                                style={styles.textInput}
                                multiline={false}
                                maxLength={13}
                                returnKeyType={'done'}
                                value={this.state.noTelp}
                                onChangeText={(text) => this.setState({ noTelp: text.replace(/[^0-9]/g, '') })}
                                placeholder={"example : 89923XXXX"}            
                            />
                        </View>

                        <TouchableOpacity
                            activeOpacity = {.7}
                            style = {styles.button}
                            onPress={() => this.addDataInstansi()}
                        >
                            <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>
                                Save
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            activeOpacity = {.7}
                            style = {styles.buttonCancel}
                            onPress={() => this.setState({
                                visibleAdd: false
                            })}
                        >
                            <Text style={{textAlign: 'center', fontSize: 15}}>
                                Cancel
                            </Text>
                        </TouchableOpacity>
                    </View>
                </Modal>
                
                <View style={{justifyContent: 'center', alignSelf: 'center', marginTop: 20}}>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>List of Instansi</Text>
                </View>

                <View style={{marginTop: 20}}>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>Sort</Text>
                </View>

                <FlatList
                    style= {{ marginTop: 20 }}
                    data= {this.state.dataInstansi == [] ? [] : this.state.dataInstansi}
                    // extraData={this.state}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({ item, index }) => {
                        return(
                            <View style={styles.frameList}>
                                <View style={{ flex: 0.5 }}>
                                    <Image source={Images.bki} style={{ height: 40, width: 70 }} />
                                </View>

                                <View style={{flex: 1.5, marginLeft: 25}}>
                                    <Text style={styles.namaInstansi}>{item.nama}</Text>
                                    <Text>{item.alamat}</Text>
                                    <Text>{item.noTelp == null || item.noTelp == '' ? '-' : item.noTelp}</Text>
                                </View>

                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignSelf:'center'}}>
                                    <Icon 
                                        name='delete' 
                                        size={20} 
                                        style={{width: 50, textAlign: 'center'}} 
                                        color='red' 
                                        onPress={() => this.deletePressed(item.nama)}
                                    />
                                    <Icon 
                                        name='edit' 
                                        style={{width: 50, textAlign: 'center'}} 
                                        size={20} 
                                        onPress={() => this.setState({ 
                                            visibleEdit: true,
                                            editName: item.nama,
                                            editAlamat: item.alamat,
                                            noTelp: item.noTelp,
                                            tempName: item.nama
                                        })}
                                    />
                                </View>
                            </View>
                        )
                    }}
                />

                <TouchableOpacity style={styles.addButton} onPress={() => this.setState({visibleAdd: true})}>
                    <Text style={styles.addButtonText}>+</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Instansi;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    frameList: {
        flexDirection: 'row', 
        justifyContent:'center', 
        alignItems: 'center', 
        marginHorizontal: 25, 
        borderWidth: 1, 
        borderColor: '#0000b7', 
        padding: 10, 
        borderRadius: 10,
        marginBottom: 20
    },
    modal: {
        width: '80%', 
        // height: '60%', 
        backgroundColor: 'white', 
        justifyContent: 'center', 
        alignSelf: 'center',
        borderRadius: 15,
        padding: 20
    },
    frame: {
        width: '100%',
        marginTop: 10
    },
    label: {
        fontSize: 15,
        marginBottom: 5
    },
    textInput: {
        width: '100%',
        fontSize: 14,
        fontStyle: 'normal',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#0000b7',
        alignSelf: 'center'
    },
    button: {
        marginTop: 20,
        width: '100%',
        padding: 10,
        backgroundColor: '#0000b7', 
        borderWidth: 1, 
        borderColor: 'white',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    buttonCancel: {
        marginTop: 20,
        width: '100%',
        padding: 10,
        borderWidth: 1, 
        borderColor: '#0000b7',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    addButton: {
        position: 'absolute',
        zIndex: 11,
        right: 20,
        bottom: 50,
        backgroundColor: '#0000b7',
        width: 70,
        height: 70,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
    },
    addButtonText: {
        color: '#fff',
        fontSize: 24,
    },
    namaInstansi: {
        fontWeight: 'bold'
    }
})