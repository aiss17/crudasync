import AsyncStorage from '@react-native-community/async-storage';
import React, { Component } from 'react'
import { BackHandler, Text, View, TouchableOpacity, Alert, StyleSheet, FlatList, Image, TextInput, ScrollView } from 'react-native'
import { NavigationEvents } from 'react-navigation'
import Images from '../../Assets/Images';
import Icon from 'react-native-vector-icons/AntDesign'
import Gender from 'react-native-vector-icons/Fontisto'
import Modal from 'react-native-modal';
import {Picker} from '@react-native-picker/picker';
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import ImagePicker from 'react-native-image-picker'

let radio_props = [
    {label: 'Laki-laki', value: 'Laki-laki' },
    {label: 'Perempuan', value: 'Perempuan' }
];
class Member extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataInstansi: [],
            dataMember: [],
            visibleEdit: false,
            visibleAdd: false,
            nama: '',
            jenisKelamin: '',
            umur:'',
            alamat: '',
            noTelp: '',
            instansi: '',
            tempNama: '',
            tempInstansi: '',
            sort: false
        }
    }

    backPressed = () => {
		Alert.alert(
            'Exit App',
            'Exiting the application?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'OK',
                onPress: () => BackHandler.exitApp()
            }, ], {
                cancelable: false
            }
        )
		return true;
    };

    deletePressed = (nama, instansi) => {
		Alert.alert(
            'Delete Member',
            'Are you sure want to delete this Member?', [{
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel'
            }, {
                text: 'Delete',
                onPress: () => this.deleteDataMember(nama, instansi)
            }, ], {
                cancelable: false
            }
        )
		return true;
    };

    componentDidMount() {
        this.getDataMember()
        this.getDataInstansis()
		BackHandler.addEventListener('hardwareBackPress', this.backPressed);
	}
	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.backPressed);
    }    

    setItem = async (key, item) => {
        try {
            await AsyncStorage.setItem(key, item);
            console.log("Data baru yang dibuat => " + await AsyncStorage.getItem(key))
            this.getDataMember()
        } catch(err) {
            console.log("Something wrong => " + err)
        }
    }

    async getDataMember() {
        let backUpListMember = await AsyncStorage.getItem("dataMember")
        console.log(await AsyncStorage.getItem('dataMember'))
        
        setTimeout(() => {
            this.setState({
                dataMember: JSON.parse(backUpListMember)
            })
        }, 1000);
    }

    async addDataMember() {
        let dataMember = await AsyncStorage.getItem('dataMember')

        console.log(await AsyncStorage.getItem('dataMember'))
        let parseData = JSON.parse(dataMember)

        if(parseData != null) {
            let indexID = parseData.findIndex((val) => val.nama.toUpperCase() == this.state.nama.toUpperCase() && val.instansi.toUpperCase() == this.state.instansi.toUpperCase())

            if (indexID != -1) {
                const checkGender = parseData[indexID].jenisKelamin
                console.log(checkGender)
                if(checkGender == 'Laki-laki'){
                    alert("His Name has been registered with the Instance!")
                } else {
                    alert("Her Name has been registered with the Instance!")
                }
            } else {
                const dataMember = 
                {
                    nama: this.state.nama,
                    jenisKelamin: this.state.jenisKelamin,
                    umur: this.state.umur,
                    alamat: this.state.alamat,
                    noTelp: this.state.noTelp,
                    instansi: this.state.instansi
                }
        
                parseData.push(dataMember)
                const dataMemberParse = JSON.stringify(parseData)
                this.setItem("dataMember", dataMemberParse)
            }
        } else {
            const dataMember = [ 
                {
                    nama: this.state.nama,
                    jenisKelamin: this.state.jenisKelamin,
                    umur: this.state.umur,
                    alamat: this.state.alamat,
                    noTelp: this.state.noTelp,
                    instansi: this.state.instansi
                }
            ]
    
            const dataMemberParse = JSON.stringify(dataMember)
            this.setItem("dataMember", dataMemberParse)
        }
        setTimeout(() => {
            this.setState({
                visibleAdd: false,
                nama: '',
                jenisKelamin: '',
                umur:'',
                alamat: '',
                noTelp: '',
                instansi: ''
            })
        }, 1000);
    }

    async putDataMember() {
        let dataMember = await AsyncStorage.getItem('dataMember')

        console.log(await AsyncStorage.getItem('dataMember'))
        let parseData = JSON.parse(dataMember)
        let indexData = parseData.findIndex((val) => val.nama.toUpperCase() == this.state.tempNama.toUpperCase() && val.instansi.toUpperCase() == this.state.tempInstansi.toUpperCase())
        console.log("Index result fo update => " + indexData)

        if (indexData != -1) {
            parseData.splice(indexData, 1)

            const dataMember = 
            {
                nama: this.state.nama,
                jenisKelamin: this.state.jenisKelamin,
                umur: this.state.umur,
                alamat: this.state.alamat,
                noTelp: this.state.noTelp,
                instansi: this.state.instansi
            }

            parseData.push(dataMember)
            const dataMemberParse = JSON.stringify(parseData)
            this.setItem("dataMember", dataMemberParse)
            setTimeout(() => {
                this.setState({
                    visibleEdit: false,
                    nama: '',
                    jenisKelamin: '',
                    umur:'',
                    alamat: '',
                    noTelp: '',
                    instansi: ''
                })
            }, 1000);
        } else {
            alert("gagal!")
        }
    }

    async deleteDataMember(nama, instansi) {
        let dataMember = await AsyncStorage.getItem('dataMember')

        console.log(await AsyncStorage.getItem('dataMember'))
        let parseData = JSON.parse(dataMember)
        let indexData = parseData.findIndex((val) => val.nama.toUpperCase() == nama.toUpperCase() && val.instansi.toUpperCase() == instansi.toUpperCase())

        parseData.splice(indexData, 1)
        const dataMemberParse = JSON.stringify(parseData)
        this.setItem("dataMember", dataMemberParse)
    }

    async getDataInstansis() {
        let backUpListInstansi = await AsyncStorage.getItem("dataInstansi")
        console.log(await AsyncStorage.getItem('dataInstansi'))
        
        if(backUpListInstansi != null){
            setTimeout(() => {
                this.setState({
                    dataInstansi: JSON.parse(backUpListInstansi)
                })
            }, 1000);
        }
    }

    takePhoto = () => {
        const options = {
            title: 'Pilh Foto',
            takePhotoButtonTitle: 'Ambil Foto',
            chooseFromLibraryButtonTitle: 'Galeri',
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            storageOptions: {
                skipBackup: true
            }
        }

        ImagePicker.showImagePicker(options, (response) => {
            console.tron.log('Response = ', response);

            if (response.didCancel) {
                console.tron.log('User cancelled image picker');
            } else if (response.error) {
                console.tron.log('ImagePicker Error: ', response.error);
            } else {
                let source = { uri: response.data };

                this.setState({ 
                    avatarSource: source.uri,
                    avatarImage: 'data:image/jpeg;base64,' + source.uri
                });

            }
        });
    }

    render() {
        return(
            <View style={styles.container}>
                <NavigationEvents            
                    onDidFocus={() => {
                        this.getDataMember(),
                        this.getDataInstansis()
                    }}
                />

                <Modal isVisible={this.state.visibleAdd} 
                    style={styles.modal}
                    onBackButtonPress={() => this.setState({ visibleAdd: false })}
                >
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ flex: 1 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'flex-start' }}>
                                <Text style={{ fontSize: 30, fontWeight: 'bold', marginRight: 5}}>Add</Text>
                                <Icon 
                                    name='addfile' 
                                    size={20} 
                                    style={{justifyContent: 'center', alignSelf: 'center'}}
                                />
                            </View>

                            <View style={[styles.frame, { marginTop: 20 }]}>
                                <Text style={styles.label}>
                                    Name
                                </Text>
                                <TextInput
                                    ref={(input) => { this.nama = input; }}
                                    keyboardType={'default'}
                                    editable={true}
                                    style={styles.textInput}
                                    multiline={false}
                                    returnKeyType={'done'}
                                    value={this.state.nama}
                                    onChangeText={(text) => this.setState({ nama: text })}
                                    placeholder={"Enter your Name..."}
                                />
                            </View>

                            <View style={styles.frame}>
                                <Text style={styles.label, {marginBottom: 10}}>
                                    Jenis Kelamin
                                </Text>
                                
                                <RadioForm
                                radio_props={radio_props}
                                initial={'Laki-Laki'}
                                formHorizontal={false}
                                labelHorizontal={true}
                                buttonColor={'#0000b7'}
                                selectedButtonColor={'#0000b7'}
                                buttonSize={10}
                                buttonOuterSize={23}
                                animation={true}
                                onPress={(value) => {
                                    this.setState({jenisKelamin:value})
                                }}
                                />
                            </View>

                            <View style={styles.frame}>
                                <Text style={styles.label}>
                                    Umur
                                </Text>
                                <TextInput
                                    ref={(input) => { this.umur = input; }}
                                    onSubmitEditing={() => { this.alamat.focus(); }}
                                    keyboardType={'phone-pad'}
                                    editable={true}
                                    style={styles.textInput}
                                    multiline={false}
                                    maxLength={3}
                                    returnKeyType={'next'}
                                    value={this.state.umur}
                                    onChangeText={(text) => this.setState({ umur: text.replace(/[^0-9]/g, '') })}
                                    placeholder={"example : 23"}
                                />
                            </View>

                            <View style={styles.frame}>
                                <Text style={styles.label}>
                                    Alamat
                                </Text>
                                <TextInput
                                    ref={(input) => { this.alamat = input; }}
                                    onSubmitEditing={() => { this.noTelp.focus(); }}
                                    keyboardType={'default'}
                                    editable={true}
                                    style={styles.textInput}
                                    multiline={false}
                                    returnKeyType={'done'}
                                    value={this.state.alamat}
                                    onChangeText={(text) => this.setState({ alamat: text })}
                                    placeholder={"Enter your address..."}
                                />
                            </View>

                            <View style={styles.frame}>
                                <Text style={styles.label}>
                                    Nomor Telepon
                                </Text>
                                <TextInput
                                    ref={(input) => { this.noTelp = input; }}
                                    keyboardType={'phone-pad'}
                                    editable={true}
                                    style={styles.textInput}
                                    multiline={false}
                                    maxLength={13}
                                    returnKeyType={'done'}
                                    value={this.state.noTelp}
                                    onChangeText={(text) => this.setState({ noTelp: text.replace(/[^0-9]/g, '') })}
                                    placeholder={"example : 89923XXXX"}            
                                />
                            </View>

                            <View style={styles.frame}>
                                <Text style={styles.label}>
                                    Instansi
                                </Text>                            

                                <View style={styles.dropDown}>
                                    <Picker
                                    selectedValue={this.state.instansi}
                                    onValueChange={(itemValue, itemIndex) =>
                                        this.setState({
                                            instansi: itemValue
                                        })
                                    }>
                                        <Picker.Item label="-Pilih Instansi-" value="" />
                                        {this.state.dataInstansi.map((item, index) => {
                                            return(                                        
                                                <Picker.Item label={item.nama} value={item.nama} key={index} />
                                            )
                                        })}
                                    </Picker>
                                </View>
                            </View>

                            <TouchableOpacity
                                activeOpacity = {.7}
                                style = {styles.button}
                                onPress={() => this.addDataMember()}
                            >
                                <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>
                                    Save
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                activeOpacity = {.7}
                                style = {styles.buttonCancel}
                                onPress={() => this.setState({
                                    visibleAdd: false
                                })}
                            >
                                <Text style={{textAlign: 'center', fontSize: 15}}>
                                    Cancel
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </Modal>

                <Modal isVisible={this.state.visibleEdit} 
                    style={styles.modal}
                    onBackButtonPress={() => this.setState({ visibleEdit: false })}
                >
                    <ScrollView showsVerticalScrollIndicator={false}>
                        <View style={{ flex: 1 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'center', alignSelf: 'flex-start' }}>
                                <Text style={{ fontSize: 30, fontWeight: 'bold', marginRight: 5}}>Edit</Text>
                                <Icon 
                                    name='edit' 
                                    size={20} 
                                    style={{justifyContent: 'center', alignSelf: 'center'}}
                                />
                            </View>

                            <TouchableOpacity
                                activeOpacity={.7}
                                onPress={this.takePhoto}
                                style={{ alignItems: 'center', marginTop: 10, marginBottom: 10 }}
                            >
                                {(isEmpty(this.state.avatarImage)) ?
                                    <ImageBackground resizeMode='stretch' source={Images.avatarKlub} style={style.image}>
                                        
                                    </ImageBackground> 
                                    :
                                    <ImageBackground resizeMode='stretch' source={{uri: this.state.avatarImage}} style={style.image} imageStyle={{borderRadius: 15}}>
                                        
                                    </ImageBackground>
                                }
                            </TouchableOpacity>

                            <View style={[styles.frame, { marginTop: 20 }]}>
                                <Text style={styles.label}>
                                    Name
                                </Text>
                                <TextInput
                                    ref={(input) => { this.nama = input; }}
                                    keyboardType={'default'}
                                    editable={true}
                                    style={styles.textInput}
                                    multiline={false}
                                    returnKeyType={'done'}
                                    value={this.state.nama}
                                    onChangeText={(text) => this.setState({ nama: text })}
                                    placeholder={"Enter your Name..."}
                                />
                            </View>

                            <View style={styles.frame}>
                                <Text style={styles.label, {marginBottom: 10}}>
                                    Jenis Kelamin
                                </Text>
                                
                                <RadioForm
                                radio_props={radio_props}
                                initial={'Laki-Laki'}
                                formHorizontal={false}
                                labelHorizontal={true}
                                buttonColor={'#0000b7'}
                                selectedButtonColor={'#0000b7'}
                                buttonSize={10}
                                buttonOuterSize={23}
                                animation={true}
                                onPress={(value) => {
                                    this.setState({jenisKelamin:value})
                                }}
                                />
                            </View>

                            <View style={styles.frame}>
                                <Text style={styles.label}>
                                    Umur
                                </Text>
                                <TextInput
                                    ref={(input) => { this.umur = input; }}
                                    onSubmitEditing={() => { this.alamat.focus(); }}
                                    keyboardType={'phone-pad'}
                                    editable={true}
                                    style={styles.textInput}
                                    multiline={false}
                                    maxLength={3}
                                    returnKeyType={'next'}
                                    value={this.state.umur}
                                    onChangeText={(text) => this.setState({ umur: text.replace(/[^0-9]/g, '') })}
                                    placeholder={"example : 23"}
                                />
                            </View>

                            <View style={styles.frame}>
                                <Text style={styles.label}>
                                    Alamat
                                </Text>
                                <TextInput
                                    ref={(input) => { this.alamat = input; }}
                                    onSubmitEditing={() => { this.noTelp.focus(); }}
                                    keyboardType={'default'}
                                    editable={true}
                                    style={styles.textInput}
                                    multiline={false}
                                    returnKeyType={'done'}
                                    value={this.state.alamat}
                                    onChangeText={(text) => this.setState({ alamat: text })}
                                    placeholder={"Enter your address..."}
                                />
                            </View>

                            <View style={styles.frame}>
                                <Text style={styles.label}>
                                    Nomor Telepon
                                </Text>
                                <TextInput
                                    ref={(input) => { this.noTelp = input; }}
                                    keyboardType={'phone-pad'}
                                    editable={true}
                                    style={styles.textInput}
                                    multiline={false}
                                    maxLength={13}
                                    returnKeyType={'done'}
                                    value={this.state.noTelp}
                                    onChangeText={(text) => this.setState({ noTelp: text.replace(/[^0-9]/g, '') })}
                                    placeholder={"example : 89923XXXX"}            
                                />
                            </View>

                            <View style={styles.frame}>
                                <Text style={styles.label}>
                                    Instansi
                                </Text>                            

                                <View style={styles.dropDown}>
                                    <Picker
                                    selectedValue={this.state.instansi}
                                    onValueChange={(itemValue, itemIndex) =>
                                        this.setState({
                                            instansi: itemValue
                                        })
                                    }>
                                        <Picker.Item label="-Pilih Instansi-" value="" />
                                        {this.state.dataInstansi.map((item, index) => {
                                            return(                                        
                                                <Picker.Item label={item.nama} value={item.nama} key={index} />
                                            )
                                        })}
                                    </Picker>
                                </View>
                            </View>

                            <TouchableOpacity
                                activeOpacity = {.7}
                                style = {styles.button}
                                onPress={() => this.putDataMember()}
                            >
                                <Text style={{color: 'white', textAlign: 'center', fontSize: 20}}>
                                    Save
                                </Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                activeOpacity = {.7}
                                style = {styles.buttonCancel}
                                onPress={() => this.setState({
                                    visibleEdit: false
                                })}
                            >
                                <Text style={{textAlign: 'center', fontSize: 15}}>
                                    Cancel
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                </Modal>
                
                <View style={{justifyContent: 'center', alignSelf: 'center', marginTop: 20}}>
                    <Text style={{fontSize: 15, fontWeight: 'bold'}}>List of Member</Text>
                </View>

                <View style={{marginTop: 20, marginHorizontal: 25, flexDirection: 'row'}}>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>Sort</Text>
                    <Text style={{fontSize: 20, marginHorizontal: 20}}
                        onPress={()=> this.setState({ sort: true })}
                    >
                        Acs
                    </Text>
                    <Text style={{fontSize: 20}}
                        onPress={()=> this.setState({ sort: false })}
                    >
                        Desc
                    </Text>
                </View>

                <FlatList
                    style= {{ marginTop: 20 }}
                    data= {this.state.dataMember == [] ? [] : this.state.sort ? this.state.dataMember.sort((a, b) => a.nama.localeCompare(b.nama)) : this.state.dataMember.sort((a, b) => b.nama.localeCompare(a.nama))}
                    // extraData={this.state}
                    keyExtractor={(item, index) => String(index)}
                    renderItem={({ item, index }) => {
                        return(
                            <View style={styles.frameList}>
                                <View style={{ flex: 0.5 }}>
                                    {/* <Image source={Images.bki} style={{ height: 40, width: 70 }} /> */}
                                    <Gender 
                                        name= {item.jenisKelamin == 'Laki-laki' ? 'male' : 'female'} 
                                        size={40} 
                                        style={{justifyContent: 'center', alignSelf: 'center'}}
                                    />
                                </View>

                                <View style={{ flex: 1.5, marginLeft: 5 }}>
                                    <Text style={styles.nama}>{item.nama.toUpperCase()}, {item.umur}</Text>
                                    <Text style={styles.intansi}>{item.instansi}</Text>
                                </View>

                                <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-end', alignSelf:'center'}}>
                                    <Icon 
                                        name='delete' 
                                        size={20} 
                                        style={{width: 50, textAlign: 'center'}} 
                                        color='red' 
                                        onPress={() => this.deletePressed(item.nama, item.instansi)}
                                    />
                                    <Icon 
                                        name='edit' 
                                        style={{width: 50, textAlign: 'center'}} 
                                        size={20} 
                                        onPress={() => this.setState({ 
                                            visibleEdit: true,
                                            nama: item.nama,
                                            jenisKelamin: item.jenisKelamin,
                                            umur: item.umur,
                                            alamat: item.alamat,
                                            noTelp: item.noTelp,
                                            instansi: item.instansi,
                                            tempNama: item.nama,
                                            tempInstansi: item.instansi
                                        })}
                                    />
                                </View>
                            </View>
                        )
                    }}
                />

                <TouchableOpacity style={styles.addButton} onPress={() => this.setState({visibleAdd: true})}>
                    <Text style={styles.addButtonText}>+</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default Member;

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    frameList: {
        flexDirection: 'row', 
        justifyContent:'center', 
        alignItems: 'center', 
        marginHorizontal: 25, 
        borderWidth: 1, 
        borderColor: '#0000b7', 
        padding: 10, 
        borderRadius: 10,
        marginBottom: 20
    },
    modal: {
        width: '80%', 
        // height: '60%', 
        backgroundColor: 'white', 
        justifyContent: 'center', 
        alignSelf: 'center',
        borderRadius: 15,
        padding: 20
    },
    frame: {
        width: '100%',
        marginTop: 10
    },
    label: {
        fontSize: 15,
        marginBottom: 5
    },
    textInput: {
        width: '100%',
        fontSize: 14,
        fontStyle: 'normal',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#0000b7',
        alignSelf: 'center'
    },
    dropDown: {
        width: '100%',
        borderWidth: 1,
        borderRadius: 10,
        borderColor: '#0000b7',
        alignSelf: 'center'
    },
    button: {
        marginTop: 20,
        width: '100%',
        padding: 10,
        backgroundColor: '#0000b7', 
        borderWidth: 1, 
        borderColor: 'white',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    buttonCancel: {
        marginTop: 20,
        width: '100%',
        padding: 10,
        borderWidth: 1, 
        borderColor: '#0000b7',
        borderRadius: 10,
        justifyContent: 'center',
        alignSelf: 'center'
    },
    addButton: {
        position: 'absolute',
        zIndex: 11,
        right: 20,
        bottom: 50,
        backgroundColor: '#0000b7',
        width: 70,
        height: 70,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
    },
    addButtonText: {
        color: '#fff',
        fontSize: 24,
    },
    nama: {
        fontWeight: 'bold'
    },
    intansi: {
        fontSize: 12
    }
})