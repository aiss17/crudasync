const Images = {
    splashScreen: require('./splash.jpg'),
    home: require('./iconHome.png'),
    list: require('./iconList.png'),
    nmw: require('./nmw.png'),
    bki: require('./logobki.jpg')
}

export default Images;